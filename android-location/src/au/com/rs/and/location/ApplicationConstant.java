package au.com.rs.and.location;

public interface ApplicationConstant {
	String LOCATION_KEY_LATITUDE = "au.com.rs.and.location.point-latitude";
	String LOCATION_KEY_LONGITUDE = "au.com.rs.and.location.point-longitude";

	String SELECTED_KEY_LATITUDE = "au.com.rs.and.location.selected-latitude";
	String SELECTED_KEY_LONGITUDE = "au.com.rs.and.location.selected-longitude";

	String BUNDLE_KEY_USER_LOCATION = "au.com.rs.and.location.user-Location-bundle";
	String BUNDLE_KEY_SELECTED_LOCATION = "au.com.rs.and.location.selected-Location-bundle";
}
