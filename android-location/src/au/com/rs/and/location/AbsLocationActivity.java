package au.com.rs.and.location;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public abstract class AbsLocationActivity {

	private static final String TAG = "AbsLocationActivity";

	public static LatLng retrieveUserLocationFrom(Bundle bundle) {
		LatLng latLng = null;

		Bundle userLocationBundle = bundle
				.getBundle(ApplicationConstant.BUNDLE_KEY_USER_LOCATION);
		if (userLocationBundle != null) {
			Log.d(TAG,
					"user loc log: "
							+ userLocationBundle
									.getDouble(ApplicationConstant.LOCATION_KEY_LATITUDE));
			latLng = new LatLng(
					userLocationBundle
							.getDouble(ApplicationConstant.LOCATION_KEY_LATITUDE),
					userLocationBundle
							.getDouble(ApplicationConstant.LOCATION_KEY_LONGITUDE));

		} else {
			latLng = new LatLng(0.0, 0.0);
		}

		return latLng;
	}

	public static LatLng retreiveSelectedLocationFrom(Bundle bundle) {
		LatLng latLng = null;

		Bundle selectedLocationBundle = bundle
				.getBundle(ApplicationConstant.BUNDLE_KEY_SELECTED_LOCATION);

		if (selectedLocationBundle != null) {
			Log.d(TAG,
					"selcted log: "
							+ selectedLocationBundle
									.getDouble(ApplicationConstant.SELECTED_KEY_LATITUDE));
			latLng = new LatLng(
					selectedLocationBundle
							.getDouble(ApplicationConstant.SELECTED_KEY_LATITUDE),
					selectedLocationBundle
							.getDouble(ApplicationConstant.SELECTED_KEY_LONGITUDE));
		} else {
			latLng = new LatLng(0.0, 0.0);
		}

		return latLng;
	}

	public static Bundle createSelectedPointBundle(double lat, double lang) {
		Bundle selectedLatlngBundle = new Bundle();
		selectedLatlngBundle.putDouble(
				ApplicationConstant.SELECTED_KEY_LATITUDE, lat);
		selectedLatlngBundle.putDouble(
				ApplicationConstant.SELECTED_KEY_LONGITUDE, lang);

		return selectedLatlngBundle;
	}

	public static Bundle createUserLocationPointBundle(double lat, double lang) {
		Bundle userLocationBundle = new Bundle();
		userLocationBundle.putDouble(ApplicationConstant.LOCATION_KEY_LATITUDE,
				lat);
		userLocationBundle.putDouble(
				ApplicationConstant.LOCATION_KEY_LONGITUDE, lang);

		return userLocationBundle;
	}
}
