package au.com.rs.and.location.model;

import android.util.Log;

public class SensorInfo {
	private double azimuth;
	private double xPitch;
	private double yPitch;

	private double inclination;

	public double getAzimuth() {
		return azimuth;
	}

	public void setAzimuth(double azimuth) {
		this.azimuth = azimuth;
	}

	public double getxPitch() {
		return xPitch;
	}

	public void setxPitch(double xPitch) {
		this.xPitch = xPitch;
	}

	public double getyPitch() {
		return yPitch;
	}

	public void setyPitch(double yPitch) {
		this.yPitch = yPitch;
	}

	public double getInclination() {
		return inclination;
	}

	public void setInclination(double inclination) {
		this.inclination = inclination;
	}

	/**
	 * convert the azimuth, radian to degree
	 * 
	 * @return
	 */
	public double headingInDegree() {
		double angle = ((float) Math.toDegrees(azimuth) + 360) % 360;
		// double angle = azimuth * 180 / 3.14159;
		Log.d("SensorInfo", "Angle: " + Math.abs(angle));
		// angle = angle < 0 ? 360 - Math.abs(angle) : angle;
		return angle;
	}
}
