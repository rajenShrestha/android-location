package au.com.rs.and.location;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

public class GoogleMapActivity extends Activity implements OnMapClickListener,
		OnMapReadyCallback {
	private String TAG = "GoogleMapActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_google_map);

		MapFragment mapFragment = (MapFragment) getFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
	}

	@Override
	public void onMapClick(LatLng point) {
		Log.d(TAG, "LAT: " + point.latitude);
		// retrieve user location
		// put the bundle to intent
		Intent intentToMain = new Intent(this.getApplicationContext(),
				MainActivity.class);
		Bundle userLocationBundle = getIntent().getExtras().getBundle(
				ApplicationConstant.BUNDLE_KEY_USER_LOCATION);
		intentToMain.putExtra(ApplicationConstant.BUNDLE_KEY_USER_LOCATION,
				userLocationBundle);

		intentToMain.putExtra(ApplicationConstant.BUNDLE_KEY_SELECTED_LOCATION,
				AbsLocationActivity.createSelectedPointBundle(point.latitude,
						point.longitude));

		Log.d(TAG, "LNG: " + point.longitude);
		startActivity(intentToMain);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		Log.d(TAG, "onStart");
		super.onStart();

		MapFragment mapFragment = (MapFragment) getFragmentManager()
				.findFragmentById(R.id.map);
		GoogleMap map = mapFragment.getMap();
		map.setMyLocationEnabled(true);

		// retrieve intent
		Intent locationIntent = getIntent();
		Bundle bundle = locationIntent.getExtras();

		if (bundle == null) {
			Log.d(TAG, "bundle is null");
			return;
		}

		LatLng latLngSelected = AbsLocationActivity
				.retreiveSelectedLocationFrom(bundle);
		
		if (latLngSelected.latitude != 0.0 && latLngSelected.longitude != 0.0) {
			map.animateCamera(CameraUpdateFactory.newLatLng(latLngSelected));
			return;
		}

		// if the selected point does not exist then used the user location
		LatLng latLngUserLocation = AbsLocationActivity
				.retrieveUserLocationFrom(bundle);
		map.animateCamera(CameraUpdateFactory.newLatLng(latLngUserLocation));
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public void onMapReady(GoogleMap map) {

		map.setOnMapClickListener(this);
	}

}
