package au.com.rs.and.location.Handler;

import android.location.Location;

import com.google.android.gms.common.ConnectionResult;

public interface UiInteractionHandler {

	void startResolution(ConnectionResult result);

	void enableComponent();

	void disableComponent();

	void updateUI(Location location);
	
	void showErrorDialog(String dialogTag, int errorCode);
}
