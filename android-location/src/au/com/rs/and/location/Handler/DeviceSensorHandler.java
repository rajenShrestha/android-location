package au.com.rs.and.location.Handler;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import au.com.rs.and.location.model.SensorInfo;

public class DeviceSensorHandler implements SensorEventListener {

	private static final String TAG = "DeviceSensorHandler";

	private final SensorManager _sensorManager;
	private final Sensor _sensorMagneticField;
	private final Sensor _sensorAcceleration;

	private SensorChangeListener sensorChangeListener;

	public DeviceSensorHandler(Context context) {
		_sensorManager = (SensorManager) context
				.getSystemService(context.SENSOR_SERVICE);
		_sensorMagneticField = _sensorManager
				.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		_sensorAcceleration = _sensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	}

	public SensorChangeListener getSensorChangeListener() {
		return sensorChangeListener;
	}

	public void setSensorChangeListener(
			SensorChangeListener sensorChangeListener) {
		this.sensorChangeListener = sensorChangeListener;
	}

	public void onStart() {
		_sensorManager.registerListener(this, _sensorAcceleration,
				SensorManager.SENSOR_DELAY_UI);
		_sensorManager.registerListener(this, _sensorMagneticField,
				SensorManager.SENSOR_DELAY_UI);
		Log.d(TAG, "onStart: ");

	}

	public void onStop() {
		_sensorManager.unregisterListener(this, _sensorAcceleration);
		_sensorManager.unregisterListener(this, _sensorMagneticField);
		Log.d(TAG, "onStop: ");
	}

	private float[] rotationMatrix = new float[9];
	private float[] inclinationMatrix = new float[9];
	private float[] valueMagneticField = new float[3];
	private float[] valueGravity = new float[3];

	private int previousHeading = 0;
	private long lastChangeDispatch = -1;

	@Override
	public void onSensorChanged(SensorEvent event) {
		Log.d(TAG, "onSensorChanged: ");
		Sensor sensor = event.sensor;

		if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
			valueMagneticField = lowPass(event.values.clone(), valueMagneticField);

		} else if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			valueGravity = lowPass(event.values.clone(), valueGravity);
		}
		if (valueGravity != null && valueMagneticField != null) {
			boolean success = SensorManager.getRotationMatrix(rotationMatrix,
					inclinationMatrix, valueGravity, valueMagneticField);
			
			Log.d(TAG, "Number of field in magnetic: "
					+ valueMagneticField.length + " : " + valueMagneticField[0]);
			Log.d(TAG, "Number of field in accelerometer: "
					+ valueGravity.length + " : " + valueGravity[2]);
			if (success) {
				// the orientation rotation array
				float[] valueOrientation = new float[3];
				SensorManager.getOrientation(rotationMatrix, valueOrientation);
				Log.d(TAG, "Device heading: " + valueOrientation[0] + " : "
						+ valueOrientation[1] + " : " + valueOrientation[2]);

				SensorInfo sensorInfo = createSensorInfo(valueOrientation);
				int heading = (int) sensorInfo.headingInDegree();
				if (Math.abs(heading - previousHeading) >= 1
						&& System.currentTimeMillis() - lastChangeDispatch > 50) {
					Log.d(TAG, "In Angle: difference > 1 : " + heading);
					sensorChangeListener.onSensorReceive(sensorInfo);
					previousHeading = heading;
					lastChangeDispatch = System.currentTimeMillis();
				}
			}
		}
	}

	private SensorInfo createSensorInfo(float[] valueOrientation) {
		SensorInfo info = new SensorInfo();
		info.setAzimuth(valueOrientation[0]);
		info.setxPitch(valueOrientation[1]);
		info.setyPitch(valueOrientation[2]);
		return info;
	}
	
	static final float ALPHA = 0.2f;
	protected float[] lowPass( float[] input, float[] output ) {
	    if ( output == null ) return input;

	    for ( int i=0; i<input.length; i++ ) {
	        output[i] = output[i] + ALPHA * (input[i] - output[i]);
	    }
	    return output;
	}

	private SensorInfo createSensorInfo(float heading) {
		SensorInfo info = new SensorInfo();
		info.setAzimuth(heading);

		return info;
	}

	private float averageAngle(float[] terms, int totalTerm) {
		float sumSin = 0;
		float sumCos = 0;
		for (int i = 0; i < totalTerm; i++) {
			sumSin += Math.sin(terms[i]);
			sumCos += Math.cos(terms[i]);
		}
		return (float) Math.atan2(sumSin / totalTerm, sumCos / totalTerm);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		Log.d(TAG, "onAccuracyChanged: ");
	}

	public static interface SensorChangeListener {
		void onSensorReceive(SensorInfo sensorInfo);
	}

}
