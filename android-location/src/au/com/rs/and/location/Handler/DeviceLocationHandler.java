package au.com.rs.and.location.Handler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class DeviceLocationHandler implements ConnectionCallbacks,
		OnConnectionFailedListener, LocationListener {

	private static final String TAG = "DeviceLocationHandler";

	private GoogleApiClient googleApiClient;
	private boolean resolvingError = false;

	private UiInteractionHandler interactionHandler;
	private Context context;

	public static final String DIALOG_ERROR = "dialog_error";
	public static int OUR_REQUEST_CODE = 9009;

	private static final LocationRequest LOCATION_REQUEST = LocationRequest.create()
			.setInterval(5000).setFastestInterval(16)
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

	public DeviceLocationHandler(UiInteractionHandler interactionHandler,
			Context context) {

		this.interactionHandler = interactionHandler;
		this.context = context;
		if (!serviceConnected()) {
			Log.d(TAG, "Google Service Api is not available");
			return;
		}

		Log.d(TAG, "Google Service Api is available: onCreate");

		googleApiClient = new GoogleApiClient.Builder(context)
				.addApi(LocationServices.API).addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).build();
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		Log.d(TAG, "onConnected");
		interactionHandler.enableComponent();

		PendingResult<Status> result = LocationServices.FusedLocationApi
				.requestLocationUpdates(googleApiClient, LOCATION_REQUEST, this);
		
		result.setResultCallback(new ResultCallback<Status>() {

			@Override
			public void onResult(Status status) {
				if (status.isSuccess()) {
					Log.d(TAG, "Successful on requesting location updates");
				} else if (status.hasResolution()) {

				}
			}
		});
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		ConnectionResult connectionResult = result;
		if (resolvingError) {
			return;
		}
		if (result.hasResolution()) {
			resolvingError = true;
			interactionHandler.startResolution(connectionResult);
		} else {
			this.interactionHandler.showErrorDialog(DIALOG_ERROR,
					connectionResult.getErrorCode());
			resolvingError = true;
		}

	}

	@Override
	public void onConnectionSuspended(int cause) {
		Log.d(TAG, "onConnectionSuspended");
		interactionHandler.disableComponent();
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.d(TAG, "onLocationChanged");
		interactionHandler.updateUI(location);
	}

	/**
	 * Google Play Service is available
	 * 
	 * @return
	 */
	public boolean serviceConnected() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(context);
		boolean exist = false;
		if (ConnectionResult.SUCCESS == resultCode) {
			Log.d(TAG, "Google Play Service is availiable");
			Log.d(TAG, "AVAlible: " + ConnectionResult.API_UNAVAILABLE);
			exist = true;
		} else {
			this.interactionHandler.showErrorDialog(DIALOG_ERROR, resultCode);
		}
		return exist;
	}

	public void activityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "On Activity Result");
		if (requestCode == OUR_REQUEST_CODE) {
			resolvingError = false;
			if (resultCode == Activity.RESULT_OK) {
				// Make sure the app is not already connected or attempting to
				// connect
				if (!googleApiClient.isConnecting()
						&& !googleApiClient.isConnected()) {
					googleApiClient.connect();
				}
			}
		}
	}

	public void startResolution(ConnectionResult result, Activity activity) {
		ConnectionResult connectionResult = result;
		if (resolvingError) {
			return;
		}
		if (connectionResult.hasResolution()) {
			try {
				resolvingError = true;
				connectionResult.startResolutionForResult(activity,
						OUR_REQUEST_CODE);
			} catch (SendIntentException e) {
				googleApiClient.connect();
			}
		} else {
			this.interactionHandler.showErrorDialog(DIALOG_ERROR,
					connectionResult.getErrorCode());
			resolvingError = true;
		}

	}

	public void connect() {
		if (!resolvingError) {
			if (googleApiClient != null) {
				googleApiClient.connect();
				Log.d(TAG, "Google Service Api is connected: on connect");
			}
		}
	}

	public void disconnect() {
		Log.d(TAG, "disconnect");
		if (googleApiClient != null) {
			googleApiClient.disconnect();
		}
	}

	public void onDialogDismissed() {
		resolvingError = false;
	}
}
