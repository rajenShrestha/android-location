package au.com.rs.and.location;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.GeomagneticField;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import au.com.rs.and.location.Handler.DeviceSensorHandler;
import au.com.rs.and.location.Handler.DeviceSensorHandler.SensorChangeListener;
import au.com.rs.and.location.Handler.DeviceLocationHandler;
import au.com.rs.and.location.Handler.UiInteractionHandler;
import au.com.rs.and.location.model.SensorInfo;
import au.com.rs.and.location.utils.Helper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.model.LatLng;

public class MainActivity extends ActionBarActivity implements
		UiInteractionHandler, SensorChangeListener {

	private static final String TAG = "MainActivity";
	private Location currentLocation;
	private LatLng selectedLatLng;
	private String locationProvider = "fused";

	private float heading = 0;

	public DeviceLocationHandler locationHandler;
	public DeviceSensorHandler deviceSensorHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		locationHandler = new DeviceLocationHandler(this,
				this.getApplicationContext());
		deviceSensorHandler = new DeviceSensorHandler(
				this.getApplicationContext());
		deviceSensorHandler.setSensorChangeListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		locationHandler.connect();
		Log.d(TAG, "About to start deviceSendorHandler");
		deviceSensorHandler.onStart();

		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		if (bundle == null) {
			return;
		}
		selectedLatLng = AbsLocationActivity
				.retreiveSelectedLocationFrom(bundle);
		Location selectedLocation = createLocation(selectedLatLng);

		Log.d(TAG, "on Start Lat: " + selectedLatLng.latitude + " Long :"
				+ selectedLatLng.longitude);
		if (selectedLatLng.latitude != 0.0) {
			TextView textViewSelected = (TextView) findViewById(R.id.textViewSelectedLocation);
			textViewSelected.setText("Lat: " + selectedLatLng.latitude
					+ " Lng: " + selectedLatLng.longitude);

			LatLng userLocationPoint = AbsLocationActivity
					.retrieveUserLocationFrom(bundle);
			Location userLocation = createLocation(userLocationPoint);

			TextView textViewHeading = (TextView) findViewById(R.id.textViewBearingAngle);
			heading = Helper.normalDegree(userLocation
					.bearingTo(selectedLocation));
			textViewHeading.setText("Heading: " + heading);

		}

	}

	private Location createLocation(LatLng latLng) {
		Location selectedLocation = new Location(locationProvider);
		selectedLocation.setLatitude(latLng.latitude);
		selectedLocation.setLongitude(latLng.longitude);

		return selectedLocation;
	}

	@Override
	public void onResume() {
		Log.d(TAG, "onResume");
		super.onResume();
		locationHandler.connect();
		deviceSensorHandler.onStart();
	}

	@Override
	public void onPause() {
		Log.d(TAG, "onPause");
		locationHandler.disconnect();
		deviceSensorHandler.onStop();
		super.onPause();

	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestory");
		locationHandler.disconnect();
		deviceSensorHandler.onStop();
		super.onDestroy();
	}

	@Override
	protected void onStop() {
		Log.d(TAG, "onStop");
		locationHandler.disconnect();
		deviceSensorHandler.onStop();
		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "On Activity Result");
		locationHandler.activityResult(requestCode, resultCode, data);
	}

	public void startResolution(ConnectionResult result) {
		locationHandler.startResolution(result, this);
	}

	public void enableComponent() {
		Button showMapButton = (Button) findViewById(R.id.buttonShowGoogleMap);
		showMapButton.setClickable(true);
	}

	public void disableComponent() {
		Button showMapButton = (Button) findViewById(R.id.buttonShowGoogleMap);
		showMapButton.setClickable(false);
	}

	public void showMap(View view) {
		Intent showMap = new Intent(this.getApplicationContext(),
				GoogleMapActivity.class);
		Log.d(TAG, "showMap: Latitude" + currentLocation.getLatitude());

		if (selectedLatLng != null) {
			showMap.putExtra(ApplicationConstant.BUNDLE_KEY_SELECTED_LOCATION,
					AbsLocationActivity.createSelectedPointBundle(
							selectedLatLng.latitude, selectedLatLng.longitude));
		}

		showMap.putExtra(ApplicationConstant.BUNDLE_KEY_USER_LOCATION,
				AbsLocationActivity.createUserLocationPointBundle(
						currentLocation.getLatitude(),
						currentLocation.getLongitude()));
		startActivity(showMap);
	}

	public void updateUI(Location location) {
		Log.d(TAG, "updateUI");
		currentLocation = location;
		Location loc = location;
		if (location.hasBearing()) {
			Log.d(TAG, "Bearing: " + loc.getBearing());
		}
		Log.d(TAG, "Alttidue: " + loc.getAltitude());
		Log.d(TAG, "Latitude: " + loc.getLatitude());
		Log.d(TAG, "Longitude: " + loc.getLongitude());

		TextView locationTextView = (TextView) findViewById(R.id.textViewLocation);
		locationTextView.setText("Lat: " + loc.getLatitude() + " Log: "
				+ loc.getLongitude());

		// Kath:
		// Latitude:27.689406°
		// Longitude:85.322668°
		// http://www.findlatitudeandlongitude.com/?loc=kathmandu%2C+thapathali&id=459934
		locationProvider = location.getProvider();
		Log.d(TAG, "Location Provider :" + locationProvider);
		Location ktmLocation = new Location(locationProvider);
		float ktmBearing = location.bearingTo(ktmLocation);
		Log.d(TAG, "Bearing: " + ktmBearing);

		GeomagneticField gmf = new GeomagneticField(Double.valueOf(
				location.getLatitude()).floatValue(), Double.valueOf(
				location.getLongitude()).floatValue(), Double.valueOf(
				location.getAltitude()).floatValue(),
				System.currentTimeMillis());
		Log.d(TAG, "Heading :" + gmf.getDeclination());

	}

	@Override
	public void showErrorDialog(String dialogTag, int errorCode) {
		ErrorDialogFragment dialogFragment = new ErrorDialogFragment(dialogTag);
		Bundle args = new Bundle();
		args.putInt(dialogTag, errorCode);

		dialogFragment.setArguments(args);
		dialogFragment.setShowsDialog(true);
	}

	private void onDialogDismissed() {
		this.locationHandler.onDialogDismissed();
	}

	public static class ErrorDialogFragment extends DialogFragment {

		private String dialogTag;

		public ErrorDialogFragment(String dialogTag) {
			this.dialogTag = dialogTag;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			int errorCode = this.getArguments().getInt(dialogTag);

			return GooglePlayServicesUtil.getErrorDialog(errorCode,
					this.getActivity(), errorCode);
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			((MainActivity) getActivity()).onDialogDismissed();
		}
	}

	int[] headings = new int[10];
	int countHeading = 0;

	@Override
	public void onSensorReceive(SensorInfo sensorInfo) {
		// headings[countHeading] = (int) sensorInfo.headingInDegree();
		// Log.d(TAG, "Length: " + headings.length + " : heading: " +
		// sensorInfo.headingInDegree());
		// countHeading = countHeading + 1;
		//
		// if (countHeading < 10) {
		// return;
		// }
		//
		// int totalHeading = 0;
		// for (int i = 0; i < headings.length; i++) {
		// Log.d(TAG, " heading: " + headings[i]);
		// totalHeading = totalHeading + headings[i];
		// }
		// int averageHeading = totalHeading / 10;
		// countHeading = 0;
		// Log.d(TAG, "Heading: " + sensorInfo.headingInDegree() +
		// " average heading: " + averageHeading);
		TextView textViewSelected = (TextView) findViewById(R.id.textViewSelectedLocation);
		textViewSelected.setText(heading + " : Azimuth: Heading: "
				+ (int) sensorInfo.headingInDegree());

	}
}